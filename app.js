/*node start file*/
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var register=require('./routes/register');
var login=require('./routes/login');
var admin=require('./routes/category');

var logObject = require("./utils/logger_util.js").logger();

var app = express();
app.set("env", "production");
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.enable("view cache");
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var http = require("http");
var server = http.createServer(app);

var domainMiddleware = require("domain-middleware");
app.use(domainMiddleware({
    server: server,
    killTimeout: 10000
}));

app.HttpServer = server;

var session=require("express-session");
app.use(session({secret:"angular",resave: false,saveUninitialized: false,cookie:{maxAge:1800000}}));

app.use('/', index);
app.use('/register',register);
app.use('/login',login);
app.use('/category',admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    logObject.error("\r\nrequest happen error url：" + req.protocol + "://" + req.headers.host + req.originalUrl + "\r\n"
        + "stack message：\r\n" + err.stack + "\r\n");
    if (/\/api/gi.test(req.originalUrl)) {
        res.send({error_code:500,error_message:"服务器繁忙，请稍后再试"});
        return false;
    }
    res.render('error', {
        message: err.message,
        error: {}
    });
});
server.listen(3001);

module.exports = app;


