/**
 * Created by ljc on 2016/2/17.
 */
module.exports.showCategory=function(req,res){
    res.render("category",{});
};
module.exports.getListByPage=function(req,res){
    var page=req.query.page;
    var pageSize=req.query.pageSize;

    var category=require("../service/categoryDataAccess.js");
    category.getPageCategory(page,pageSize,function(err,data){
        res.send({recordNumber:data[0][0].recordNumber,result:data[1]});
    });
};