/**
 * Created by wqy0126 on 2016/2/17.
 */
module.exports.showLogin= function (req, res) {
    res.render("login", {});
};
module.exports.login=function(req,res){
    var userModel = req.body || {};
    if (!userModel.userName) {
        res.send({error: 402, message: "请输入用户名！"});
        return false;
    }
    if (!userModel.password) {
        res.send({error: 403, message: "请输入用户密码！"});
        return false;
    }
    var loginDataAccess = require("../service/loginDataAccess.js");
    loginDataAccess.checkLogin(userModel, function (err, data) {
        if (err) {
            res.send({error: 500, message: "网络繁忙，请稍后再试！"});
            return false;
        }
        if (data && data.length == 1) {
            if (data[0].LoginName !== userModel.userName) {
                res.send({error: 401, message: "用户名或密码不正确！"});
                return false;
            }
            if (data[0].Password !== userModel.password) {
                res.send({error: 401, message: "用户名或密码不正确！"});
                return false;
            }
            res.send({error: 200, message: "登录成功！"});
            return true;
        } else {
            res.send({error: 404, message: "用户不存在！"});
            return false;
        }
    });
};
