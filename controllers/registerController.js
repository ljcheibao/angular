/**
 * Created by ljc on 2016/2/17.
 */
var PW = require("png-word");
var pw = PW(PW.GRAY);
//var r = require("random-word")("abcdefghijklmnopqrst0123456789");
var random = require("random-js")();

module.exports.showRegister=function(req,res){
    res.render('register', {});
};

module.exports.getValidateCode=function(req,res){
    var value = random.integer(1000, 9999);
    pw.createPNG(value, function (pngnum) {
        req.session["validcode"] = value;
        res.send(pngnum);
    })
};

module.exports.register=function(req,res){
    var register = req.body;
    if (parseInt(register.validCode) == req.session["validcode"]) {
        var registerDataAccess = require("../service/registerDataAccess.js");
        var registerModel = {};
        registerModel.Email = register.email;
        registerModel.UserName = register.showName;
        registerModel.LoginName = register.loginName;
        registerModel.Password = register.password;
        registerModel.LastLoginTime = new Date();
        registerModel.IsValid = true;

        var ipAddress;
        var forwardedIpsStr = req.header('x-forwarded-for');
        if (forwardedIpsStr) {
            var forwardedIps = forwardedIpsStr.split(',');
            ipAddress = forwardedIps[0];
        }
        if (!ipAddress) {
            ipAddress = req.connection.remoteAddress;
        }
        registerModel.LoginIp = ipAddress;

        var async = require("async");
        async.waterfall([
            function (callback) {
                registerDataAccess.checkRegisterEmail(registerModel.Email, function (err, data) {
                    if (!err && data[0].email <= 0) {
                        callback(null, "newemail");
                    } else {
                        if (err) {
                            callback(null, "err");
                        } else {
                            callback(null, {error: 300, message: "该邮箱已经注册！"});
                        }
                    }
                });
            },
            function (args, callback) {
                if (args == "newemail") {
                    registerDataAccess.checkRegisterUser(registerModel.LoginName, function (err, data) {
                        if (!err && data[0].loginName <= 0) {
                            callback(null, "newuser");
                        } else {
                            if (err) {
                                callback(null, "err");
                            } else {
                                callback(null, {error: 400, message: "该用户已经注册！"});
                            }
                        }
                    });
                } else {
                    callback(null, args);
                }
            },
            function (args, callback) {
                if (args == "newuser") {
                    registerDataAccess.registUser(registerModel, function (err, data) {
                        if (!err) {
                            callback(null, "success");
                        } else {
                            callback(null, "false");
                        }
                    });
                } else {
                    callback(null, args);
                }
            }
        ], function (err, result) {
            if (!err && result == "success") {
                res.send({error: 200, message: "注册成功！"});
                return true;
            } else {
                req.session["validcode"] = "";
                if (err || err == "err") {
                    res.send({error: 500, message: "网络繁忙，请稍后再试！"});
                    return false;
                }
                res.send(result);
                return true;
            }
        });
    } else {
        res.send({error: 100, message: "验证码输入错误！"});
        req.session["validcode"] = "";
        return true;
    }
};