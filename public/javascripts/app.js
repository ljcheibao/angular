/**
 * Created by ljc on 2015/7/7.
 */
require.config({
    baseUrl:"javascripts/",
    paths:{
        'jquery':['lib/jquery-1.8.2.min'],
        'underscore':['lib/underscore'],
        'text':['lib/text'],
        'angular':['lib/angular'],
        'angular-route':['angular-route']
    },
    skim:{
        'jquery':{
            exports:'$'
        },
        'underscore':{
            exports:'_'
        },
        'angular':{
            exports:'angular'
        },
        'text':{
            exports:'text'
        },
        'angular-route':{
            exports:"angular-route",
            deps: ["angular"]
        }
    },
    urlArgs:"v=11"
});

require(['routes/routes','angular','jquery','text','underscore'],function(route){
    var target = location.pathname;
    var match = null,
        re = null;
    for (var key in route.route) {
        re = new RegExp("^" + key + "$", "gi");
        if (re.test(target)) {
            match = route.route[key];
            break;
        }
    }
    if (!match) {
        match = route.defaultRoute;
    }
    if ($.type(match) === "string") {
        match = [match];
        require(match);
    }else{
        if(match.hasOwnProperty("module")){
            var _loadApp=[match["module"]];
            require(_loadApp,function(){
                angular.module(match["appDomain"],["controllers"]);
                angular.bootstrap(document,[match["appDomain"]]);
            });
        }
    }
});