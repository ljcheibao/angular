/**
 * Created by ljc on 2015/7/13.
 */
define(["controllers/controllers","services/category/categoryService","directives/common/pagedirective"],function(controllers){
    controllers.controller("categoryController",["$scope","$sce",function($scope,$sce){
        $scope.mynames="<li>第三方的算法</li>";
        $scope.trustHtml = function() {
            return $sce.trustAsHtml($scope.mynames);
        };
    }]);
});