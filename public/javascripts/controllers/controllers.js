/**
 * Created by ljc on 2015/7/7.
 */
define(["services/services","directives/directives"],function(){
    return angular.module("controllers",["services","directives"]);
});