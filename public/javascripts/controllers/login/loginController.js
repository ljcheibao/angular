/**
 * Created by wqy0126 on 2016/1/10.
 */
define([
    "controllers/controllers",
    "services/login/loginService"
],function(controllers){
    var loginUser={
        validateUserData:function(userModel){
            if(!userModel.userName&&!userModel.password){
                userModel.userNameError="用户名不能为空";
                userModel.passwordError="登录密码不能为空";
                userModel.isShowUserNameError=true;
                userModel.isShowPasswordError=true;
                return false;
            }
            if(!userModel.userName){
                userModel.userNameError="用户名不能为空";
                userModel.isShowUserNameError=true;
                return false;
            }
            if(!userModel.password){
                userModel.passwordError="登录密码不能为空";
                userModel.isShowPasswordError=true;
                return false;
            }
            return true;
        }
    };
    controllers.controller("loginController",["$scope","loginServer",function($scope,loginServer){
        var userModel={
            userName:"",
            userNameError:"",
            isShowUserNameError:false,
            password:"",
            passwordError:"",
            isShowPasswordError:false
        };
        $scope.userModel=userModel;
        $scope.login=function(){
            delete userModel.userNameError;
            delete userModel.isShowUserNameError;
            delete userModel.passwordError;
            delete userModel.isShowPasswordError;
            var valid=loginUser.validateUserData(userModel);
            if(valid){
                loginServer.checkLogin(userModel,function(result){
                    switch (result.error){
                        case 200:
                            window.location="/category";
                            break;
                        case 401:
                        case 404:
                        case 402:
                            userModel.userNameError=result.message;
                            userModel.isShowUserNameError=true;
                            break;
                        case 403:
                            userModel.passwordError=result.message;
                            userModel.isShowPasswordError=true;
                            break;
                        case 500:
                            break;
                    }
                });
            }
        };
    }]);
});