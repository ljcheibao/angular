/**
 * Created by ljc on 2015/10/8.
 */
define(["controllers/controllers","services/ngRepeat/ngRepeatService"],function(controllers){
    controllers.controller('ngRepeatController', ['$scope', 'ngRepeatDataService', function ($scope, ngRepeat) {
        $scope.ngRepeatItems =ngRepeat;
    }]);
});