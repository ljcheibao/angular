/**
 * Created by ljc on 2015/7/9.
 */
define(["controllers/controllers","services/register/registerService"], function (controllers) {
    controllers.controller("registerController", ["$scope", "registerService", function ($scope, registerService) {
        $scope.validcodeUrl = "/register/validcode";
        var register = {
            email: "",
            emailStatus: false,
            emailMsg: "",
            loginName: "",
            loginNameStatus: false,
            loginNameMsg: "",
            showName: "",
            password: "",
            confirmPassword: "",
            passwordStatus: false,
            passwordMsg: "",
            validCode: "",
            validCodeStatus: false,
            validCodeMsg: ""
        };
        $scope.register = register;
        $scope.check = function (act) {
            switch (act) {
                case "email":
                    if (register.email == "") {
                        register.emailStatus = true;
                        register.emailMsg = "请输入注册邮箱！";
                        return false;
                    } else {
                        var hasRegister = false;
                        /*后台判断该邮箱是否已注册*/
                        if (hasRegister) {
                            register.emailStatus = true;
                            register.emailMsg = "该邮箱已有用户注册，请换一个邮箱重新注册！";
                            return false;
                        }else{
                            register.emailStatus = false;
                        }
                    }
                    break;
                case "password":
                    if(register.password==""){
                        register.passwordStatus=true;
                        register.passwordMsg="请输入登陆密码！";
                        return false;
                    }
                    if(register.password!=register.confirmPassword){
                        register.passwordStatus=true;
                        register.passwordMsg="两次密码不一致！";
                        return false;
                    }else{
                        register.passwordStatus=false;
                    }
                    break;
            }
        };
        $scope.refresh = function () {
            $scope.validcodeUrl = $scope.validcodeUrl + "?tt=" + new Date().getTime();
        };
        $scope.registUser = function () {
            var result=true;
            if (register.email == "") {
                register.emailStatus = true;
                register.emailMsg = "请输入注册邮箱！";
                result=false;
            } else {
                var hasRegister = false;
                /*后台判断该邮箱是否已注册*/
                if (hasRegister) {
                    register.emailStatus = true;
                    register.emailMsg = "该邮箱已有用户注册，请换一个邮箱重新注册！";
                    result=false;
                }else{
                    register.emailStatus = false;
                }
            }

            if(register.loginName==""){
                register.loginNameStatus=true;
                register.loginNameMsg="请输入用户登陆名！";
                result=false;
            }else{
                var hasRegister=false;
                /*后台判断该用户名是否已注册*/
                if(hasRegister){
                    register.loginNameStatus=true;
                    register.loginNameMsg="该用户名已经被注册，请换一个用户名重新注册！";
                    result=false;
                }else{
                    register.loginNameStatus=false;
                }
            }

            if(register.password==""){
                register.passwordStatus=true;
                register.passwordMsg="请输入登陆密码！";
                result=false;
            }
            if(register.password!=register.confirmPassword){
                register.passwordStatus=true;
                register.passwordMsg="两次密码不一致！";
                result=false;
            }

            if(register.validCode==""){
                register.validCodeStatus=true;
                register.validCodeMsg="请输入验证码！";
                result=false;
            }
            if(!result) return false;
            registerService.registerFn(register,function(data,status,headers,config){
                register.validCode="";
                switch (data.error){
                    case 200:
                        window.location="/category";
                        break;
                    case 100:
                    case 500:
                        register.validCodeStatus=true;
                        register.validCodeMsg=data.message;
                        break;
                    case 300:
                        register.emailStatus=true;
                        register.emailMsg=data.message;
                        break;
                    case 400:
                        register.loginNameStatus=true;
                        register.loginNameMsg=data.message;
                        break;
                }
            })
        }
    }]);
});