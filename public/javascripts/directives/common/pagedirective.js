/**
 * Created by ljc on 2015/7/13.
 */
define(["directives/directives"], function (directives) {
    directives.directive("page", function () {
        var renderPage = {
            conf: {
                totalPage: 1,
                currentPage: 1,
                showPageNumber: 11
            },
            render: function (act) {
                var pages = [];
                var index = 0;
                pages.push("<li><a href='javascript:void(0);' ng-click='clickPage(\"first\")'>首页</a></li>");
                pages.push("<li><a  href='javascript:void(0);' aria-label='Previous' ng-click='clickPage(\"pre\")'><span aria-hidden='true'>«</span></a></li>");
                switch (act) {
                    case "first":
                        for (var i = 1; i <= this.conf.totalPage; i++) {
                            if (i <= this.conf.showPageNumber) {
                                pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==" + i + "]'><a href='javascript:void(0);' ng-click='clickEveryPage(" + i + ")'>" + i + "</a></li>");
                            } else {
                                if (index == 0 && this.conf.totalPage > this.conf.showPageNumber) {
                                    index++;
                                    pages.push("<li class='disabled'><a href='javascript:void(0);'>...</a></li>");
                                }
                                if (i == this.conf.totalPage) {
                                    pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==" + i + "]'><a href='javascript:void(0);' ng-click='clickEveryPage(" + i + ")'>" + i + "</a></li>");
                                }
                            }
                        }
                        break;
                    case "last":
                        for (var i = 1; i <= this.conf.totalPage; i++) {
                            if (i >= this.conf.totalPage - this.conf.showPageNumber + 1 && i <= this.conf.totalPage && this.conf.totalPage > this.conf.showPageNumber) {
                                if (index == 0) {
                                    index++;
                                    pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==1]'><a href='javascript:void(0);' ng-click='clickEveryPage(1)'>1</a></li>");
                                    pages.push("<li class='disabled'><a href='javascript:void(0);'>...</a></li>");
                                }
                                pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==" + i + "]'><a href='javascript:void(0);' ng-click='clickEveryPage(" + i + ")'>" + i + "</a></li>");
                            }
                        }
                        break;
                    case "pre":
                        pages.push(pushPageStr("pre"));
                        break;
                    case "next":
                        pages.push(pushPageStr("next"));
                        break;
                    default:
                        pages.push(pushPageStr());
                        break;
                }
                pages.push("<li><a  href='javascript:void(0);' aria-label='Previous' ng-click='clickPage(\"next\")'><span aria-hidden='true'>»</span></a></li>");
                pages.push("<li><a href='javascript:void(0);' ng-click='clickPage(\"last\")'>尾页</a></li>");

                return pages.join('');
            }
        };

        function pushPageStr(act) {
            var nextPage = renderPage.conf.currentPage + 1;
            var next5Page = nextPage + 5;
            var pre5Page = nextPage - 5;
            var pages = [];
            var index = 0;
            switch (act) {
                case "next":
                    break;
                case "pre":
                    var nextPage = renderPage.conf.currentPage - 1;
                    var next5Page = nextPage + 5;
                    var pre5Page = nextPage - 5;
                    break;
                default:
                    nextPage = renderPage.conf.currentPage;
                    next5Page = renderPage.conf.currentPage + 5;
                    pre5Page = renderPage.conf.currentPage - 5;
                    break;
            }
            if (renderPage.conf.showPageNumber > renderPage.conf.totalPage - pre5Page + 1) {
                pre5Page = renderPage.conf.totalPage - renderPage.conf.showPageNumber + 1;
            }

            for (var i = pre5Page; i <= nextPage; i++) {
                if (i <= 0) continue;
                if (pre5Page > 1) {
                    if (index == 0) {
                        index++;
                        pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==1]'><a href='javascript:void(0);' ng-click='clickEveryPage(1)'>1</a></li>");
                        pages.push("<li class='disabled'><a href='javascript:void(0);'>...</a></li>");
                    }
                    pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==" + i + "]'><a href='javascript:void(0);' ng-click='clickEveryPage(" + i + ")'>" + i + "</a></li>");
                } else {
                    pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==" + i + "]'><a href='javascript:void(0);' ng-click='clickEveryPage(" + i + ")'>" + i + "</a></li>");
                }
            }
            if (next5Page < renderPage.conf.showPageNumber) {
                next5Page = renderPage.conf.showPageNumber;
            }

            index = 0;
            for (var i = nextPage + 1; i <= next5Page; i++) {
                if (i <= renderPage.conf.totalPage) {
                    pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==" + i + "]'><a href='javascript:void(0);' ng-click='clickEveryPage(" + i + ")'>" + i + "</a></li>");
                }
            }
            if (next5Page < renderPage.conf.totalPage) {
                if (index == 0) {
                    index++;
                    pages.push("<li class='disabled'><a href='javascript:void(0);'>...</a></li>");
                    pages.push("<li ng-class='{true:\"active\"}[conf.currentPage==" + renderPage.conf.totalPage + "]'><a href='javascript:void(0);' ng-click='clickEveryPage(" + renderPage.conf.totalPage + ")'>" + renderPage.conf.totalPage + "</a></li>");
                }
            }

            return pages.join('');
        }

        return {
            restrict: 'EA',
            transclude: true,
            controller: [ "$scope", "$compile", "$sce", "dataService", function ($scope, $compile, $sce, dataService) {
                var conf = {
                    recordNumber: 0,
                    totalPage: 1,
                    currentPage: 1,
                    pageSize: 5
                };
                $scope.conf = conf;
                $scope.dataService = dataService;
                $scope.complie = $compile;

                /* $scope.pages=renderPage.render("first");
                 $scope.trustHtml = function() {
                 return $sce.trustAsHtml($scope.pages);
                 };*/
            }],
            //template:"<ul class='pagination' ng-bind-html='trustHtml()'></ul>",
            link: function (scope, iElem, iAttrs, controller) {
                scope.dataService.loadDataFn(scope.conf.currentPage, scope.conf.pageSize, function (list) {
                    scope.items = list.data;
                    scope.conf.recordNumber = list.recordNumber;
                    scope.conf.totalPage = Math.ceil(scope.conf.recordNumber / scope.conf.pageSize);
                    renderPage.conf.totalPage = scope.conf.totalPage;
                    renderPage.conf.currentPage = 1;

                    scope.pages = scope.complie(renderPage.render("first"))(scope);
                    var $ul = angular.element("<ul>");
                    $ul.attr("class", "pagination");
                    $ul.append(scope.pages);
                    iElem.append($ul);

                    scope.clickPage = function (act) {
                        switch (act) {
                            case "first":
                                scope.conf.currentPage = 1;
                                renderPage.conf.currentPage = 1;
                                break;
                            case "last":
                                scope.conf.currentPage = scope.conf.totalPage;
                                renderPage.conf.currentPage = scope.conf.totalPage;
                                break;
                            case "pre":
                                scope.conf.currentPage = scope.conf.currentPage - 1;
                                renderPage.conf.currentPage = scope.conf.currentPage + 1;
                                break;
                            case "next":
                                scope.conf.currentPage = scope.conf.currentPage + 1;
                                renderPage.conf.currentPage = scope.conf.currentPage - 1;
                                break;
                        }
                        if (renderPage.conf.currentPage <= 1 && act == "pre") {
                            scope.conf.currentPage = 1;
                            renderPage.conf.currentPage = 1;
                            return false;
                        }
                        if (renderPage.conf.currentPage >= scope.conf.totalPage && act == "next") {
                            scope.conf.currentPage = scope.conf.totalPage;
                            renderPage.conf.currentPage = scope.conf.totalPage;
                            return false;
                        }
                        $ul.children().remove();
                        $ul.append(scope.complie(renderPage.render(act))(scope));

                        scope.dataService.loadDataFn(scope.conf.currentPage, scope.conf.pageSize, function (list) {
                            scope.items = list.data;
                        });
                    };
                    scope.clickEveryPage = function (page) {
                        scope.conf.currentPage = page;
                        renderPage.conf.currentPage = page;
                        $ul.children().remove();
                        $ul.append(scope.complie(renderPage.render())(scope));

                        scope.dataService.loadDataFn(scope.conf.currentPage, scope.conf.pageSize, function (list) {
                            scope.items = list.data;
                        });
                    };
                });
            },
            replace: true
        };
    });
});