/**
 * Created by ljc on 2015/7/9.
 */
define(["directives/directives"],function(directives){
    directives.directive("warnDirective",function(){
        return{
            restrict: "EA",
            scope: {
                msg: '@'
            },
            template: '<div class="alert alert-warning" style="padding: 6px;" role="alert">{{msg}}</div>',
            replace: true
        };
    });
});