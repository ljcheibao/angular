/**
 * Created by ljc on 2015/7/7.
 */
define(function () {
    var route = {
        "\/login.*": {module:"controllers/login/app",appDomain:"webApp"},
        "\/register.*": {module:"controllers/register/app",appDomain:"webApp"},
        "\/category.*": {module:"controllers/category/app",appDomain:"webApp"},
        "\/ng-repeat.*": {module:"controllers/ngRepeat/app",appDomain:"webApp"}
    };
    return {
        route: route,
        defaultRoute: {module:"controllers/admin/app",appDomain:"webApp"}
    };
});