/**
 * Created by ljc on 2015/7/13.
 */
define(["services/services"], function (services) {
    services.factory("dataService", ["$http",function ($http) {
        return{
            loadDataFn: function (page, pageSize,callback) {
                pageSize = pageSize ? pageSize : 10;
                $.ajax({
                    type:"Get",
                    url: "/category/page?page="+page+"&pageSize="+pageSize,
                    dataType:"json",
                    success:function(data){
                        var result= {
                            recordNumber:data.recordNumber,
                            data:data.result
                        };
                        callback(result);
                    },
                    error:function(){

                    }
                });
//                $http({
//                    method: "GET",
//                    url: "/category/page?page="+page+"&pageSize="+pageSize,
//                    responseType: "json"
//                }).success(function (data, status, headers, config) {
//                    var result= {
//                        recordNumber:data.recordNumber,
//                        data:data.result
//                    };
//                    callback(result);
//                }).error(function (data, status, headers, config) {
//                    callback({});
//                });
            }
        };
    }]);
});