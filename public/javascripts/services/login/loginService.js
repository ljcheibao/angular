/**
 * Created by wqy0126 on 2016/1/10.
 */
define(["services/services"],function(services){
    services.factory("loginServer",["$http",function($http){
        return {
            checkLogin:function(userModel,callback){
                $http({
                    method:"Post",
                    url:"/login",
                    data:userModel,
                    responseType:"json"
                }).success(function(data, status, headers, config){
                    callback(data, status, headers, config);
                }).error(function(data, status, headers, config){
                    callback({error:500,message:"网络繁忙，请稍后再试！"}, status, headers, config);
                });
            }
        };
    }]);
});
