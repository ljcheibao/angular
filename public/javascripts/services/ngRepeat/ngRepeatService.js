/**
 * Created by ljc on 2015/10/8.
 */
define(["services/services"],function(services){
    return services.factory("ngRepeatDataService",function(){
        return[
            {id:"ngRepeat1001",name:"手机",list:[
                {id:1001,name:"IPhone5S",quantity:1,price:4500,totalPrice:4500},
                {id:1002,name:"IPhone6",quantity:1,price:5000,totalPrice:5000},
                {id:1003,name:"IPhone6 puls",quantity:1,price:5800,totalPrice:5800}
            ]},
            {id:"ngRepeat1002",name:"超级手机",list:[
                {id:1004,name:"IPhone5S",quantity:1,price:4500,totalPrice:4500},
                {id:1005,name:"IPhone6",quantity:1,price:5000,totalPrice:5000}
            ]}
        ];
    });
});