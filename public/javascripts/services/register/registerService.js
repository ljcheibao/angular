/**
 * Created by ljc on 2015/7/10.
 */
define(["services/services"],function(services){
    services.factory("registerService",["$http",function($http){
        return {
           registerFn:function(register,callback){
               $http({
                   method: "POST",
                   url: "/register/user",
                   data: register,
                   responseType: "json"
               }).success(function (data, status, headers, config) {
                   callback(data, status, headers, config);
               }).error(function (data, status, headers, config) {
                   callback({error:500,message:"网络繁忙，请稍后再试"}, status, headers, config);
               });
           }
        }
    }]);
});