/**
 * Created by wqy0126 on 2015/7/11.
 */
var express=require("express");
var router=express.Router();

var CategoryController=require("../controllers/categoryController.js");

router.get("/",CategoryController.showCategory);

router.get("/page",CategoryController.getListByPage);

module.exports=router;