var express = require('express');
var router = express.Router();
var IndexController=require("../controllers/indexController");

/* GET home page. */
router.get('/', IndexController.showMain);

router.get('/ng-repeat',IndexController.showNgRepeat);

module.exports = router;
