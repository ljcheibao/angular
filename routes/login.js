/**
 * Created by wqy0126 on 2015/7/10.
 */
var express = require("express");
var router = express.Router();

var LoginController=require("../controllers/loginController.js");

router.get("/", LoginController.showLogin);
router.post("/", LoginController.login);

module.exports = router;