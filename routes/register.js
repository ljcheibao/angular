/**
 * Created by ljc on 2015/7/9.
 */
var express = require('express');
var router = express.Router();

var RegisterController=require("../controllers/registerController.js");

router.get('/', RegisterController.showRegister);

router.get("/validcode", RegisterController.getValidateCode);

router.post("/user", RegisterController.register);

module.exports = router;