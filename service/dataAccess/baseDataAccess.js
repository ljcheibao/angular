/**
 * Created by ljc on 2015/7/9.
 */
var baseDataAccess=(function(){
    var connObject=require("./sqlHelper.js");
    return{
        query:function(sql,parameter,trans,callback){
            connObject.excuteSql(sql,parameter,trans,function(err,data){
                console.log(data);
                if(!err){
                    callback.call(this,err,data);
                }else{
                    callback.call(this,err,null);
                }
            })
        },
        insert:function(sql,parameter,trans,callback){
            connObject.excuteSql(sql,parameter,trans,function(err,data) {
                console.log(data);
                if (!err) {
                    callback.call(this, err, data.serverStatus);
                } else {
                    callback.call(this, err, 0);
                }
            });
        },
        update:function(sql,parameter,trans,callback){
            connObject.excuteSql(sql,parameter,trans,function(err,data) {
                console.log(data);
                if (!err) {
                    callback.call(this, err, data.affectedRows);
                } else {
                    callback.call(this, err, 0);
                }
            });
        },
        delete:function(sql,parameter,trans,callback){
            connObject.excuteSql(sql,parameter,trans,function(err,data) {
                console.log(data);
                if (!err) {
                    callback.call(this, err, data.affectedRows);
                } else {
                    callback.call(this, err, 0);
                }
            });
        }
    }
})();

module.exports=baseDataAccess;