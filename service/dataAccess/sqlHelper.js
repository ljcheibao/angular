/**
 * Created by ljc on 2015/7/9.
 */
var sqlHelper = (function () {
    var mysql = require("mysql");
    var fs = require('fs');
    var path = require("path");
    var file = path.join(__dirname, "config.json");
    console.log(file);
    var option;
    var pool;
    var conn;
    //读取配置文件 获取连接池信息
    option = fs.readFileSync(file, 'utf-8');
    pool = mysql.createPool(JSON.parse(option));
    return {
        excuteSql: function (sql, parameter, isTrans, callback) {
            pool.getConnection(function (err, con) {
                if (err) {
                    console.log(err);
                    callback.call(con, err, null);
                    con.release();
                }
                if (!isTrans) {
                    con.query(sql, parameter, function (err, data) {
                        if (err) {
                            console.log(err);
                            callback.call(con, err, data);
                            con.release();
                        }
                        callback.call(con, err, data);
                        con.release();
                    });
                } else {
                    con.beginTransaction(function (err) {
                        if (err) {
                            console.log(err);
                            callback.call(con, err, null);
                            con.release();
                        }
                        con.query(sql, parameter, function (err, data) {
                            if (err) {
                                con.rollback(function() {
                                    console.log(err);
                                    callback.call(con, err, null);
                                    con.release();
                                });
                            }
                        });
                        con.commit(function(err,data) {
                            if (err) {
                                con.rollback(function() {
                                    console.log(err);
                                    callback.call(con, err, null);
                                    con.release();
                                });
                            }
                            callback.call(con, err, data);
                            con.release();
                        });
                    });
                }
            });
        }
    }
})();
module.exports = sqlHelper;