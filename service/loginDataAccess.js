/**
 * Created by ljc on 2016/1/12.
 */
var baseDataAccess=require("./dataAccess/baseDataAccess.js");
var login={
    checkLogin:function(userModel,callback){
        var querySql="select LoginName,Password from blog_user where LoginName=? and Password=?";
        var parameters=[userModel.userName,userModel.password];
        //userModel={LoginName:userModel.userName,Password:userModel.password};
        baseDataAccess.query(querySql,parameters,false,function(err,data){
            callback(err,data);
        });
    }
};
exports=module.exports=login;