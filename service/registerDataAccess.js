/**
 * Created by ljc on 2015/7/10.
 */
var register=(function(){
       var baseDataAccess=require("./dataAccess/baseDataAccess.js");
       return{
           checkRegisterEmail:function(email,callback){
               var querySql="select count(*) as email from blog_user where Email=?";
               var parameters=[email];
               baseDataAccess.query(querySql,parameters,false,function(err,data){
                   callback(err,data);
               });
           },
           checkRegisterUser:function(loginName,callback){
               var querySql="select count(*) as loginName from blog_user where LoginName=?";
               var parameters=[loginName];
               baseDataAccess.query(querySql,parameters,false,function(err,data){
                   callback(err,data);
               });
           },
           registUser:function(obj,callback){
               var insertSql="INSERT INTO blog_user SET ?";
               baseDataAccess.insert(insertSql,obj,false,function(err,data){
                   callback(err,data);
               });
           }
       };
})();

exports=module.exports=register;