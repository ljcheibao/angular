/**
 * http utility
 */
var merge = require('merge');
var async = require('async');
var request = require('request');

var fs = require('fs');
var url = require('url');

var http = require('http');
var parser = require('url');

var logger = require('./logger_util').logger();

var needle = require("needle");
var queryString = require("querystring");

var HttpUtil = {
    logFailure: function (url, res, body, params, requestBody) {
        if (res.statusCode != 200) {
            console.log('\n\n\n\n----------------------------------------------------------');
            console.log('request url: ' + url);
            console.log('status: ' + res.statusCode);
            console.log('headers: \r\n' + JSON.stringify(res.headers, null, 2));
            console.log('querystring parameters：\r\n' + JSON.stringify(params, null, 2));
            console.log('form data: \r\n' + JSON.stringify(requestBody, null, 2));
            console.log('response data: \r\n' + body);
            console.log('----------------------------------------------------------\n\n\n\n');
        }
    },
    sendRequest: function (url, req, callback) {
        try {
            var option = merge({data: {}, url: url}, {method: "Get"});
            console.log(option);
            this.sendRemoteRequest(req, option, callback);
            return true;
        } catch (e) {
            logger.info("error：\r\n" + JSON.stringify(e, null, 2) + "\r\n");
            callback.call(callback, e, {error_code: 500, message: "服务器繁忙，请稍后再试22"}, null);
            return false;
        }
    },
    sendRemoteRequest: function (req, options, callback) {
        try {
            var params = req.query || req.body;
            var opts = {};
            options.contentType = options.contentType || 'application/json;charset=utf-8';
            opts.method = options.method || 'post';
            opts.headers = {
                "content-type": options.contentType,
                "cookie": req.headers["cookie"],
                "User-Agent": req.headers["user-agent"]
            };
            opts.url = options.url;
            opts.form = options.data || {};
            var result = false;
            if (options.data) {
                for (var name in options.data) {
                    result=true;
                }
            }
            if (options.method.toLowerCase() == "get" && result) {
                if(opts.url.indexOf('?')>-1)
                    opts.url = opts.url+"&"+queryString.stringify(options.data);
                else
                    opts.url = opts.url+"?"+queryString.stringify(options.data);
                opts.form={};
            }
            logger.info("request api url：" + opts.url);
            logger.info("request headers：\r\n" + JSON.stringify(opts.headers, null, 2) + "\r\n");
            logger.info("request parameters：\r\n" + JSON.stringify(req.query || req.body, null, 2) + "\r\n");
            logger.info("request body：\r\n" + JSON.stringify(opts.form, null, 2) + "\r\n");
            request(opts, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    //try {
                    logger.info("response headers：\r\n" + JSON.stringify(response.headers, null, 2) + "\r\n");
                    var responseData = JSON.parse(body);
                    if (!responseData.error_code) {
                        responseData.error_code = 500;
                        responseData.error_message = "服务器繁忙，请稍后再试";
                        logger.info("response error api data：\r\n" + JSON.stringify(responseData, null, 2) + "\r\n");
                    } else {
                        logger.info("response api data：\r\n" + JSON.stringify(responseData, null, 2) + "\r\n");
                    }

                    callback.call(callback, null, responseData, response);
                    return true;
                    //} catch (e) {
                    //logger.info("error：\r\n" + JSON.stringify(e, null, 2) + "\r\n");
                    //callback.call(callback, error, {error_code: 500, message: "服务器繁忙，请稍后再试"}, null);
                    //}

                } else {
                    logger.info("error：\r\n" + JSON.stringify(error, null, 2) + "\r\n");
                    callback.call(callback, error, {error_code: 500, message: "服务器繁忙，请稍后再试"}, response);
                    return false;
                }
            });
        } catch (e) {
            logger.info("error：\r\n" + JSON.stringify(e, null, 2) + "\r\n");
            callback.call(callback, error, {error_code: 500, message: "服务器繁忙，请稍后再试"}, null);
            return false;
        }
    },
    getResponse: function (req, res, option, callbackFn) {
        if (!option || option.length <= 0) {
            callbackFn.call(callbackFn, "request config is wrong", null);
            return false;
        }
        var _this = this;
        var waterfall = [];
        for (var i = 0; i < option.length; i++) {
            var url = "";
            var keyValue = null;
            if (Object.prototype.toString.call(option[i]) == "[object Object]") {
                for (var key in option[i]) {
                    url = option[i][key];
                    keyValue = key;
                }
            }
            if (Object.prototype.toString.call(option[i]) == "[object String]") {
                url = option[i];
            }
            if (!/(http|https):/gi.test(url))
                url = req.protocol + "://" + req.headers.host + "/" + url;

            var fn = (function (url, keyValue) {
                return function (args, args1, callback) {
                    if (i > 0 && args == "err") {
                        callbackFn("err", null);
                        return false;
                    }
                    if (typeof args != "function" && args.error_code != 200) {
                        callbackFn(null, args);
                        return false;
                    } else {
                        _this.sendRequest(url, req, function (err, data, response) {
                            if (!err) {
                                if (keyValue) {
                                    var value = {};
                                    value[keyValue] = data;
                                    if (typeof args == "function") {
                                        value = merge({}, value);
                                    } else {
                                        value = merge(args, value);
                                    }
                                    if (typeof args == "function")
                                        args(null, value, response);
                                    if (typeof callback == "function")
                                        callback(null, value, response);
                                } else {
                                    if (typeof args == "function") {
                                        data = merge({}, data);
                                    } else {
                                        data = merge(args, data);
                                    }
                                    if (typeof args == "function")
                                        args(null, data, response);
                                    if (typeof callback == "function")
                                        callback(null, data, response);
                                }
                            } else {
                                if (typeof args == "function")
                                    args(null, "err");
                                if (typeof callback == "function")
                                    callback(null, "err");
                            }
                        });
                    }
                };
            })(url, keyValue);
            waterfall.push(fn);
        }
        async.waterfall(waterfall, function (err, result, response) {
            if (req.query.mode == "debug") {
                res.send(result);
                return false;
            }
            if (err != "err") {
                callbackFn.call(callbackFn, null, result, response);
                return true;
            } else {
                callbackFn.call(callbackFn, err, null);
                return false;
            }
        });
    },
    getParallelResponse: function (req, res, option, paralle, callbackFn) {
        paralle = paralle ? true : false;
        if (!option || Object.prototype.toString.call(option) != "[object Object]") {
            callbackFn.call(callbackFn, "request config is wrong", null);
            return false;
        }
        var _this = this;
        var parallel = {};
        for (var key in option) {
            if (!key || !option[key]) {
                callbackFn.call(callbackFn, "request config is wrong", null);
                return false;
            }
            var url = "";
            var keyValue = null;
            url = option[key];
            keyValue = key;
            if (!/(http|https):/gi.test(url))
                url = req.protocol + "://" + req.headers.host + "/" + url;
            var fn = (function (url) {
                return function (callback) {
                    var option = merge({data: {}, url: url}, {method: "Get"});
                    _this.sendRemoteRequest(req, option, function (err, data, response) {
                        //_this.sendRequest(url, req, function (err, data, response) {
                        if (!err) {
                            callback(null, {data: data, response: response});
                        } else {
                            callback(null, {data: {error_code: 500, message: "服务器繁忙，请稍后再试"}});
                        }
                    });
                };
            })(url);
            parallel[keyValue] = fn;
        }
        async.parallel(parallel, function (err, result, response) {
            var responseResult = {}, responseObject;
            for (var key in result) {
                if (result[key]["data"].error_code != 200 && paralle) continue;
                if (!responseObject) responseObject = result[key]["response"];
                responseResult = merge(responseResult, result[key]["data"]);
            }
            if (req.query.mode == "debug") {
                res.send(responseResult);
                return false;
            }
            callbackFn.call(callbackFn, null, responseResult, responseObject);
        });
    },
    sendFileToServer: function (req, option, callback) {
        var data = {
            Filedata: {
                file: option.fileName,
                content_type: 'image/jpeg'
            }
        };
       /* array(1) {
            ["Filedata"]=>
            array(5) {
                ["name"]=>
                string(28) "I5WSwnQPXycUhycufZZdo7SJ.jpg"
                    ["type"]=>
                string(9) "image/jpg"
                    ["tmp_name"]=>
                string(14) "/tmp/phpUPLceE"
                    ["error"]=>
                int(0)
                    ["size"]=>
                int(879394)
            }
        }*/
        var opts={ multipart: true };
        opts.headers = {
            "Content-Type":req.headers["content-type"],
            "cookie": req.headers["cookie"],
            "User-Agent": req.headers["user-agent"]
        };
        logger.info("request api url：" + option.url);
        logger.info("request headers：\r\n" + JSON.stringify(opts.headers, null, 2) + "\r\n");
        logger.info("request parameters：\r\n" + JSON.stringify(req.query || req.body, null, 2) + "\r\n");
        needle.post(option.url, data, opts, function (err, resp, body) {
            logger.info("response headers：\r\n" + JSON.stringify(resp.headers, null, 2) + "\r\n");
            logger.info("response error api data：\r\n" + JSON.stringify(body, null, 2) + "\r\n");
            callback(err, body, resp);
            return true;
        });
    }
};
exports = module.exports = HttpUtil;
