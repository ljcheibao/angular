/**
 * Created by ljc on 2015/11/5.
 */
var log4js = require('log4js');
var fs=require("fs");
log4js.configure({
    appenders: [
        /*{//控制台输出
            type: 'console',
            category: "console"
        },*/
        {//日期文件格式
            type: "dateFile",
            filename: 'logs/log.log',
            pattern: "_yyyy-MM-dd",
            alwaysIncludePattern: false,
            category: 'dateFileLog'
        }
    ],
    replaceConsole: true
});

var clearLogs = function () {
    var folder_exists = fs.existsSync('logs');
    if (folder_exists == true) {
        var dirList = fs.readdirSync('logs');
        var len=dirList.length;
        dirList.forEach(function (fileName) {
            if(/log.log_.*/gi.test(fileName)){
               if(len>2){
                   len--;
                   fs.unlinkSync('logs/' + fileName);
               }
            }
        });
    }
};

var interval=setInterval(function(){
    clearLogs();
},7200000);

var loggerConstructor = function (name) {
    name = name || "dateFileLog";
    var logger = log4js.getLogger(name);
    logger.setLevel('INFO');
    return logger;
};
exports.logger = loggerConstructor;
