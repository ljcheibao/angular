/**
 * Created by ljc on 2015/5/22.
 */
var Utils=(function () {
    var tools = function (selector, context) {
    };
    tools.prototype.format = function (fmt, objDate) {
        var o = {
            "M+": objDate.getMonth() + 1, //月份
            "d+": objDate.getDate(), //日
            "h+": objDate.getHours(), //小时
            "m+": objDate.getMinutes(), //分
            "s+": objDate.getSeconds(), //秒
            "q+": Math.floor((objDate.getMonth() + 3) / 3), //季度
            "S": objDate.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (objDate.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };
    tools.prototype.isLeapYear = function (objDate) {//判断闰年
        var pYear = objDate.getFullYear();
        if (!isNaN(parseInt(pYear))) {
            if ((pYear % 4 == 0 && pYear % 100 != 0) || (pYear % 100 == 0 && pYear % 400 == 0)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };
    /***************************************************
     *比较两个日期的间隔
     *参数说明:
     *objDate：结束日期
     *interval要返回的两个日期的间隔,比如：
     *s：返回两个日期相差的秒数
     *n：返回两个日期相差的分钟数
     *h：返回两个日期相差的小时数
     *d：返回两个日期相差的天数
     *w：返回两个日期相差的周数
     *m：返回连个日期相差的月数
     *y：返回连个日期相差的年数
     ****************************************************/
    tools.prototype.dateDiff = function (interval, objBeginDate, objEndDate) {
        var dtBegin = new Date(objBeginDate);
        var dtEnd = new Date(objEndDate);
        if (isNaN(dtEnd)) return undefined;
        switch (interval) {
            case "s":
                return parseInt((dtEnd - dtBegin) / 1000);
            case "n":
                return parseInt((dtEnd - dtBegin) / 60000);
            case "h":
                return parseInt((dtEnd - dtBegin) / 3600000);
            case "d":
                return parseInt((dtEnd - dtBegin) / 86400000);
            case "w":
                return parseInt((dtEnd - dtBegin) / (86400000 * 7));
            case "m":
                return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtBegin.getFullYear()) * 12) - (dtBegin.getMonth() + 1);
            case "y":
                return dtEnd.getFullYear() - dtBegin.getFullYear();
        }
    };
    tools.prototype.createCorrectDate = function (strFormatDate, dateArr) {
        if (strFormatDate == undefined || strFormatDate == null || strFormatDate == '') return 'invalidDate';
        var dateObj;
        try {
            var dateArray;
            if (strFormatDate.indexOf('-') > -1) {
                dateArray = strFormatDate.toString().split('-');
                dateObj = new Date(Number(dateArray[0]), Number(dateArray[1]) - 1, Number(dateArray[2]));
                if (Number(dateArray[0]) != dateObj.getFullYear())
                    return 'invalidDate';
                if (Number(dateArray[1]) != dateObj.getMonth() + 1)
                    return 'invalidDate';
                if (Number(dateArray[2]) != dateObj.getDate())
                    return 'invalidDate';
            } else if (strFormatDate.indexOf('.') > -1) {
                dateArray = strFormatDate.toString().split('.');
                dateObj = new Date(Number(dateArray[0]), Number(dateArray[1]) - 1, Number(dateArray[2]));
                if (Number(dateArray[0]) != dateObj.getFullYear())
                    return 'invalidDate';
                if (Number(dateArray[1]) != dateObj.getMonth() + 1)
                    return 'invalidDate';
                if (Number(dateArray[2]) != dateObj.getDate())
                    return 'invalidDate';
            } else if (strFormatDate.indexOf('/') > -1) {
                dateArray = strFormatDate.toString().split('/');
                dateObj = new Date(Number(dateArray[0]), Number(dateArray[1]) - 1, Number(dateArray[2]));
                if (Number(dateArray[0]) != dateObj.getFullYear())
                    return 'invalidDate';
                if (Number(dateArray[1]) != dateObj.getMonth() + 1)
                    return 'invalidDate';
                if (Number(dateArray[2]) != dateObj.getDate())
                    return 'invalidDate';
            }
            dateArr = dateArr || [];
            for (var i = 0; i < dateArr.length; i++) {
                switch (i) {
                    case 0:
                        dateObj.setHours(dateArr[0]);
                        if (dateObj.getHours() != Number(dateArr[0]))
                            return 'invalidDate';
                        break;
                    case 1:
                        dateObj.setMinutes(dateArr[1]);
                        if (dateObj.getMinutes() != Number(dateArr[1]))
                            return 'invalidDate';
                        break;
                    case 2:
                        dateObj.setSeconds(dateArr[2]);
                        if (dateObj.getSeconds() != Number(dateArr[2]))
                            return 'invalidDate';
                        break;
                    case 3:
                        dateObj.setMilliseconds(dateArr[3]);
                        if (dateObj.getMilliseconds() != Number(dateArr[3]))
                            return 'invalidDate';
                    default:
                        break;
                }
            }
            if (dateArr.length <= 0) {
                dateObj.setHours(0);
                dateObj.setMinutes(0);
                dateObj.setSeconds(0);
                dateObj.setMilliseconds(0);
            }
            return dateObj;
        } catch (e) {
            return 'invalidDate';
        }
    };
    tools.prototype.trim=function(value){
        var regEx=/(^\s*|\s*$)$/gi;
        return value.replace(regEx,"");
    };
    tools.prototype.unique=function (arr) {
        var res = [];
        var json = {};
        for (var i = 0; i < arr.length; i++) {
            if (!json[arr[i]]) {
                res.push(arr[i]);
                json[arr[i]] = 1;
            }
        }
        return res;
    };
    tools.prototype.clone=function(o) {
        try {
            var k, ret = o, b;
            if (o && ((b = (o instanceof Array)) || o instanceof Object)) {
                ret = b ? [] : {};
                for (k in o) {
                    if (o.hasOwnProperty(k)) {
                        ret[k] = tools.prototype.clone(o[k]);
                    }
                }
            }
            return ret;
        } catch (e) {
            throw "对象拷贝失败!";
        }
    };
    tools.prototype.isNumeric=function(obj){
        return !isNaN( parseFloat(obj) ) && isFinite( obj );
    };
    var utils = new tools();

    return utils;
})();

module.exports=Utils;